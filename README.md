# Update ES

Script para actualizar indices de ES de un conjunto de instancias de Boxer.

### Que hacer luego de clonado
- Crear un archivo *conf.py* en base a *conf_template.py*

- Crear un archivo *update_index.sh* en base a *update_index_template.sh*

- Otorgar permisos a *update_index.sh* con `chmod +u+x update_index.sh`

- Agregar al archivo de crontab la linea `0 3 * * 1-6 bash <path_to_repo>/update_index.sh`

### Glosario *conf.py*
- `BASE_DIR` es el directorio contenedor de las aplicaciones

- `DIRECTORIES` es un array con los nombres de los directorios a actualizar

-  `MAIL_CONF.sender` direccion de correo que enviara el log del proceso

-  `MAIL_CONF.password` password de correo que enviara el log del proceso

-  `MAIL_CONF.receiver` direccion de correo que recibira el log del proceso
