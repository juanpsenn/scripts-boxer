from conf import *
from sender import send_mail
from utils import *

logs = []
for directory in DIRECTORIES:
    path = BASE_DIR + directory + '/src'
    os.chdir(path)
    try:
        log(path, 'Checking index state', logs)
        check_index_state(path, logs)
    except Exception as err:
        log(path, f'[error] {str(err)}', logs)