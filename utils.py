import datetime
import os
import time

import pexpect


# Updating app stuff

def pull():
    pull = pexpect.spawn('git pull')
    pull.timeout = 1000
    pull.expect('Username *')
    pull.sendline('juanpsenn')
    pull.expect('Password *')
    pull.sendline('1@Ju1997')
    pull.expect(pexpect.EOF)
    return pull.before.decode('utf-8').split('\r\n')[-2] == 'Already up to date.'


def collectstatic(logs):
    statics = pexpect.spawn('python3 manage.py collectstatic')
    statics.expect('Type *')
    statics.sendline('yes')
    statics.expect(pexpect.EOF)
    logs[directory].append([str(datetime.now()),
                            "Done!" if 'files copied' in statics.before.decode('utf-8') else "Failed collecstatic"])


def run_build(path, logs):
    os.chdir(path + '/frontend')
    logs[directory].append([str(datetime.now()),
                            'Running build'])
    os.system('npm run build')
    os.chdir(path + '/src')
    logs[directory].append([str(datetime.now()),
                            'Running migrations and collectstatic'])


# ES index update utils

def delete_index(path, logs):
    try:
        delete_index = pexpect.spawn('python3 manage.py search_index --delete')
        delete_index.timeout = 1000
        delete_index.expect('Are you*')
        delete_index.sendline('y')
        delete_index.expect(pexpect.EOF)
        log(path, 'indice eliminado', logs)
    except Exception as err:
        log(path, f'[error] indice no eliminado {str(err)}', logs)


def create_index(path, logs):
    try:
        os.system('python3 manage.py search_index --create')
        log(path, 'indice creado', logs)
    except Exception as err:
        log(path, f'[error] indice no creado {str(err)}', logs)


def populate_index(path, logs):
    try:
        start_time = time.time()
        os.system('python3 manage.py rebuild_index')
        log(path, f'indice poblado en {time.time() - start_time}s', logs)
    except Exception as err:
        log(path, f'[error] indice no poblado {str(err)}', logs)


def check_index_state(path, logs):
    try:
        os.system('python3 manage.py check_indexes')
        log(path, 'Index checked', logs)
    except Exception as err:
        log(path, f'[error] {str(err)}', logs)


def log(path, msg, logs):
    logs.append(f'{datetime.datetime.now()}-{path}: {msg}')
