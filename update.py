from conf import *
from sender import send_mail
from utils import *

logs = []
for directory in DIRECTORIES:
    path = BASE_DIR + directory + '/src'
    os.chdir(path)
    try:
        log(path, 'inicio de proceso de reindexacion', logs)
        delete_index(path, logs)
        create_index(path, logs)
        populate_index(path, logs)
        log(path, 'proceso de reindexacion finalizado', logs)
    except Exception as err:
        log(path, f'[error] indice no reindexado {str(err)}', logs)

send_mail('\n'.join(map(str, logs)), 'Log actualizacion de indices ES.')
